﻿Imports Interpreter

Public Class NumericExpression
    Implements IExpression

    Private _value As String

    Public Sub New(token As String)
        _value = token
    End Sub
    Public Sub interpret(context As Context) Implements IExpression.interpret
        context.setOperator(context.getInteger(_value))
        context.calculate()

    End Sub
End Class
