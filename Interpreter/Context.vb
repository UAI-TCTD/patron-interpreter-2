﻿Public Class Context

    Private _nextOp As String = ""
    Private _operator As Integer = 0
    Private _result As Integer = 0

    Public Function getInteger(input As String)
        Select Case input.ToLower()

            Case "cero"
                Return 0

            Case "uno"
                Return 1
            Case "dos"
                Return 2
            Case "tres"
                Return 3
            Case "cuatro"
                Return 4
            Case "cinco"
                Return 5
            Case "seis"
                Return 6
            Case "siete"
                Return 7
            Case "ocho"
                Return 8
            Case "nueve"
                Return 9
            Case Else
                Return -1


        End Select
    End Function

    Public Sub setOperator(op As Integer)
        _operator = op
    End Sub

    Public Sub setOperation(operation As String)
        If operation.ToLower().Equals("mas") Then
            _nextOp = "+"
        ElseIf operation.ToLower().Equals("menos") Then
            _nextOp = "-"
        End If
    End Sub

    Public Sub calculate()
        If _nextOp.Equals("+") OrElse _nextOp.Equals("") Then
            _result += _operator


        ElseIf _nextOp.Equals("-") Then
            _result -= _operator

        End If
    End Sub

    Public Function getResult() As Integer
        Return _result

    End Function


End Class
