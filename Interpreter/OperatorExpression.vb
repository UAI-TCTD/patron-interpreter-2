﻿Imports Interpreter

Public Class OperatorExpression
    Implements IExpression

    Dim _operation As String
    Public Sub New(token As String)
        _operation = token
    End Sub
    Public Sub interpret(context As Context) Implements IExpression.interpret

        context.setOperation(_operation)

    End Sub
End Class
