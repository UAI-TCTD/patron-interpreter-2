﻿Imports Interpreter

Module Module1

    Sub Main()
        Dim tree As String()
        Dim context As New Context
        Dim expressions As New List(Of IExpression)
        Console.WriteLine("ingrese la operacion en letras: ")
        Dim val As String = Console.ReadLine()
        tree = val.Split(" ")

        Dim exp As IExpression
        For Each t In tree
            If context.getInteger(t) >= 0 Then
                exp = New NumericExpression(t)
            Else
                exp = New OperatorExpression(t)
            End If

            exp.interpret(context)
        Next

        Console.WriteLine("El resultado para '" & val & "' es " & context.getResult())
        Console.ReadKey()

    End Sub

End Module
